import { ContentFields, KeyValueMap } from 'contentful-management'


export interface SubField {
    field: ContentFields<KeyValueMap>, 
    type:string, 
    linkField: string
}