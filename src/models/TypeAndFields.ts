import { ContentType } from '@contentful/app-sdk';
import { ExportField } from './ExportField';

export interface TypeAndFields {
  type: ContentType | undefined;
  fields: ExportField[] | undefined;
}
