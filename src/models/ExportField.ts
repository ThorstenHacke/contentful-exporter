import { ContentFields, KeyValueMap } from 'contentful-management';

export interface ExportField {
  field: ContentFields<KeyValueMap>;
  export: boolean;
  filter: string;
}
