import { ContentType } from '@contentful/app-sdk';
import { Collection, Locale, LocaleProps } from 'contentful-management';

export interface BaseData {
  types: ContentType[];
  locales: Collection<Locale, LocaleProps>;
}
