import { ClientAPI, Entry, Environment, QueryOptions } from "contentful-management";
import { ExportField } from "../models/ExportField";
import { PageAppSDK } from "@contentful/app-sdk";
import { SubField } from "../models/SubField";

export const getAllEntries = async (environment: Environment, content_type: string, filters?: {matchKey: string, matchValue:string}[], page = 0): Promise<Entry[]> => {
    const options : QueryOptions = {
      content_type: content_type,
      skip: page*100,
      limit:100,
    };
    if(filters){
      for(const filter of filters){
        options[filter.matchKey] = filter.matchValue
      }
    }
    const entries = await environment.getEntries(options);
    const items = entries.items;
    if(entries.total >= (page+1)*100){
      items.push(...(await getAllEntries(environment, content_type, filters, page+1)));
    }
    return items;
  }

export const getFilterMatches = (fields: ExportField[]) =>  {
  return fields.filter(f => f.filter).map(f => ({
    matchKey: `fields.${f.field.id}[match]`,
    matchValue: f.filter,
  }));
}

export const getDataFromContentful = async (cma: ClientAPI, sdk: PageAppSDK, subFields: SubField[], typeId: string, filters: { matchKey: string; matchValue: string; }[] | undefined) => {
  const environment = await getContentfulEnvironment(cma, sdk);
  const subTypeEntryMap: { [key: string]: Entry[]; } = await createSubTypeEntryMap(subFields, environment);
  const entries = await getAllEntries(environment, typeId, filters);
  return { entries, subTypeEntryMap };
}

const createSubTypeEntryMap = async (subFields: SubField[], environment: any) => {
  const subTypeEntryMap: { [key: string]: Entry[]; } = {};
  if (subFields.length > 0) {
    const allSubTypes = subFields.map(sf => sf.type);
    const unique = allSubTypes.filter((value, index, array) => array.indexOf(value) === index);
    for (const subType of unique) {
      const subTypeEntries = await getAllEntries(environment, subFields[0].type);
      subTypeEntryMap[subType] = subTypeEntries;
    }
  }
  return subTypeEntryMap;
}

const getContentfulEnvironment = async (cma: ClientAPI, sdk: PageAppSDK) => {
  const space = await cma.getSpace(sdk.ids.space);
  const environment = await space.getEnvironment(sdk.ids.environment);
  return environment;
}