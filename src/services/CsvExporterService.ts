import { TypeAndFields } from "../models/TypeAndFields";
import { SubField } from "../models/SubField";
import { getDataFromContentful, getFilterMatches } from "./ContentfulTools";
import { ClientAPI, Entry } from "contentful-management";
import { PageAppSDK } from "@contentful/app-sdk";
import { ExportField } from "../models/ExportField";

const LineBreak = "\n";
const FieldSeparator = ";"
export const generateCsv = async (cma: ClientAPI, sdk: PageAppSDK, type: TypeAndFields, subFields: SubField[], locale: string) => {
    if (type.type) {
      const filters = type.fields ? getFilterMatches(type.fields): undefined;
      const { entries, subTypeEntryMap } = await getDataFromContentful(cma, sdk, subFields, type.type.sys.id, filters);
      
      const exportedFields = type.fields?.filter(f => f.export)??[];
      const header = createHeaderLine(exportedFields, subFields);
      const exportFields = entries.map(entry => createExportLine(entry, exportedFields, locale, subFields, subTypeEntryMap));

      return header + LineBreak + exportFields.join(LineBreak);
    }
  };

function createExportLine(entry: Entry, exportedFields: ExportField[], locale: string, subFields: SubField[], subTypeEntryMap: { [key: string]: Entry[]; }) {
  const directFields = entry.sys.id + FieldSeparator + (exportedFields.map(field => {
    return saveGetFieldValue(entry, field.field.id, locale);
  }).join(FieldSeparator));

  const subfields = subFields.map(subField => {
    const subTypeEntries = subTypeEntryMap[subField.type];
    const subFieldLinkId = getSubFieldLinkId(entry, subField, locale);
    const subEntry = getSubEntry(subTypeEntries, subFieldLinkId);
    return subEntry? saveGetFieldValue(subEntry, subField.field.id, locale): "";
  }).join(FieldSeparator);
  return directFields + FieldSeparator + subfields;
}

function saveGetFieldValue(entry: Entry, fieldId: string, locale: string) {
  const entryField = entry.fields[fieldId];
  return entryField ? JSON.stringify(entryField[locale]) : "";
}

function getSubFieldLinkId(entry: Entry, sf: SubField, locale: string){
  const linkField = entry.fields[sf.linkField];
  const id = linkField ? linkField[locale].sys.id: undefined;
  return id;
}

function getSubEntry(subTypeEntries: Entry[], subFieldLinkId: string){

  const foundSub = subTypeEntries.find(sub => sub.sys.id === subFieldLinkId);
  return foundSub;
}

function createHeaderLine(exportedFields: ExportField[], subFields: SubField[]) {
  return "id"
    + FieldSeparator 
    + exportedFields?.map(field => field.field.name).join(FieldSeparator) 
    + FieldSeparator 
    + subFields.map(subField => subField.field.name).join(FieldSeparator);
}




