import { PageAppSDK } from "@contentful/app-sdk";
import { BaseData } from "../models/BaseData";
import { ClientAPI } from "contentful-management";


export const loadData = async (sdk: PageAppSDK, cma: ClientAPI) : Promise<BaseData> => {
    const space = await cma.getSpace(sdk.ids.space)
  
    const environment = await space.getEnvironment(sdk.ids.environment)
    const locales = await environment.getLocales()
    const contentTypes = await environment.getContentTypes();
    const retVal: BaseData = {
      types: contentTypes.items,
      locales: locales,
    }
    return retVal;
  }
