import { PromiseFn } from "react-async";
import { BaseData } from "../models/BaseData";
import { loadData } from "./BaseDataService";
import { PageAppSDK } from "@contentful/app-sdk";
import { mockCma, mockSdk } from "../../test/mocks";

jest.mock("react-async");
jest.mock("../models/BaseData");

describe('loadBaseData', () => {
  it('should expose a function', () => {
		expect(loadData).toBeDefined();
	});
  
  it('loadBaseData should return expected output', async () => {
    mockCma.getSpace = () => ({
      getEnvironment: () => ({
        getContentTypes: () => ({
          items: null
        }),
        getLocales: () => null
      })
    });
    const retValue = await loadData(mockSdk, mockCma);
    expect(retValue.locales).toBeNull();
  });
});