import React, { useState } from 'react';
import { Select, Button, Table, Form, CopyButton, Box, TextInput, Tooltip, Text } from '@contentful/f36-components';
import { PageAppSDK } from '@contentful/app-sdk';
import { useSDK } from '@contentful/react-apps-toolkit';
import { createClient } from 'contentful-management'
import { PromiseFn, useAsync } from "react-async"
import { Textarea, FormControl } from '@contentful/f36-forms';
import { ExportField } from '../models/ExportField';
import { TypeAndFields } from '../models/TypeAndFields';
import { loadData } from '../services/BaseDataService';
import { SubField } from '../models/SubField';
import {generateCsv} from '../services/CsvExporterService'
import { BaseData } from '../models/BaseData';

const Page = () => {
  const [type, setType] = useState<TypeAndFields>();
  const [exportedCsv, setExportedCsv] = useState("");
  const [locale, setLocale] = useState("");
  const [subFields, setSubFields] = useState<SubField[]>([])
  const sdk = useSDK<PageAppSDK>();
  const cma = createClient(
    { apiAdapter: sdk.cmaAdapter },
  )

  const loadBaseData: PromiseFn<BaseData> = async ({ sdk, cma })=> {
    return loadData(sdk, cma);
  }
  const model = useAsync({
    promiseFn: loadBaseData,
    sdk: sdk,
    cma: cma
  });

  const exportButtonClicked = async () => {
    if(type) {
      const result = await generateCsv(cma, sdk, type, subFields, locale)
      if(result){
        setExportedCsv(result);
      }
    }
  };

  const selectContentType = (selectedType: string) => {
    const seltype = model.data?.types.find(t => t.sys.id === selectedType);
    const selFields = seltype?.fields.map(f => ({ field: f, export: false, filter: "" }));
    setType({ type: seltype, fields: selFields});
  }

  const changeExport = (field: ExportField) => {
    if (type) {
      field.export = !field.export
      setType({ ...type });
    }
  }

  const changeSubExport = (value: SubField) => {
    let newSubFields = [...subFields];
    const existingIndex = newSubFields.indexOf(value);
    if(existingIndex >= 0){
      newSubFields.splice(existingIndex,1)
    }else{
      newSubFields.push(value);
    }
    setSubFields(newSubFields);
  }

  const changeFilter = (field: ExportField, value: string) => {
    if (type) {
      field.filter = value
      setType({ ...type });
    }
  }

  return <Form style={{ padding: "20px" }}>
    <FormControl>
      <FormControl.Label>ContentModel</FormControl.Label>
      <Select onChange={e => selectContentType(e.target.value)}>
        <Select.Option value="none">none</Select.Option>
        {model.data?.types.map(t => <Select.Option key={t.sys.id} value={t.sys.id}>{t.name}</Select.Option>)}
      </Select>
      <FormControl.HelpText>Please select a ContentModel to export</FormControl.HelpText>
    </FormControl>
    <FormControl>
      <FormControl.Label>Locale</FormControl.Label>
      <Select value={locale} onChange={e => setLocale(e.target.value)}>
        <Select.Option value="none">none</Select.Option>
        {model.data?.locales.items.map(t => <Select.Option key={t.code} value={t.code}>{t.code}</Select.Option>)}
      </Select>
      <FormControl.HelpText>Please select a locale to export</FormControl.HelpText>
    </FormControl>
    <FormControl>
      <FormControl.Label>Fields</FormControl.Label>
      <Table>
        <Table.Head>
          <Table.Row>
            <Table.Cell>Name</Table.Cell>
            <Table.Cell>Type</Table.Cell>
            <Table.Cell>Required</Table.Cell>
            <Table.Cell>Export</Table.Cell>
            <Table.Cell><Tooltip placement="top" id="tooltip-1" content="When set, filters the entries, where the field contaisn teh given value"><Text>Filter</Text></Tooltip></Table.Cell>
          </Table.Row>
        </Table.Head>
        <Table.Body>
          {type && type.fields ? type.fields.map((field, index) =>
          {
            if(field.field.type === 'Link' && field.field.linkType === 'Entry'){
              const linkType = field.field.validations?.find(v => v.linkContentType)?.linkContentType?.find(x => x);
              if(linkType){
                const subType = model.data?.types.find(t => t.sys.id === linkType);
                if(subType){
                   return <>{subType.fields.map((subfield, subindex)  => {
                    return <Table.Row key={subindex}>
                    <Table.Cell><Box marginLeft="spacingM">{subfield.id || 'Untitled'}</Box></Table.Cell>
                    <Table.Cell>{linkType+" - "+subfield.type}</Table.Cell>
                    <Table.Cell>{subfield.required ? "YES" : "NO"}</Table.Cell>
                    <Table.Cell><input type="checkbox" checked={!!subFields.find(s => s.field.id === subfield.id && s.linkField === field.field.id)} onChange={() => changeSubExport({field: subfield, type:linkType, linkField: field.field.id})}></input></Table.Cell>
                    <Table.Cell></Table.Cell>
                    </Table.Row>
                  })}
                  </>
                }
              }
              // return <></>
              return <Table.Row key={index}>
              <Table.Cell>{field.field.name || 'Untitled'}</Table.Cell>
              <Table.Cell>{field.field.type}</Table.Cell>
              <Table.Cell>{field.field.required ? "YES" : "NO"}</Table.Cell>
              <Table.Cell><input type="checkbox" checked={field.export} onChange={() => changeExport(field)}></input></Table.Cell>
              <Table.Cell><TextInput value={field.filter} onChange={(e) => changeFilter(field, e.target.value)}></TextInput></Table.Cell>
            </Table.Row>
            }else{
            return <Table.Row key={index}>
              <Table.Cell>{field.field.name || 'Untitled'}</Table.Cell>
              <Table.Cell>{field.field.type}</Table.Cell>
              <Table.Cell>{field.field.required ? "YES" : "NO"}</Table.Cell>
              <Table.Cell><input type="checkbox" checked={field.export} onChange={() => changeExport(field)}></input></Table.Cell>
              <Table.Cell><TextInput value={field.filter} onChange={(e) => changeFilter(field, e.target.value)}></TextInput></Table.Cell>
            </Table.Row>
            }
          }
          ) : <></>}
        </Table.Body>
      </Table>
      <FormControl.HelpText>Select the fields you want to export</FormControl.HelpText>
    </FormControl>
    <FormControl>

      <Button variant='primary' onClick={() => exportButtonClicked()}>Export</Button>
    </FormControl>
    <FormControl>
      {exportedCsv && <>
        <FormControl.Label>Exported CSV</FormControl.Label>
        <Box>
          <Textarea rows={10} isReadOnly={true} value={exportedCsv} ></Textarea>
        </Box>
        <FormControl.HelpText><CopyButton value={exportedCsv}></CopyButton></FormControl.HelpText>

      </>}
    </FormControl>
  </Form>

};

export default Page;
